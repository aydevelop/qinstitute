﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using QueryInstitute.Models;
using System.Collections.Generic;
using System.Linq;

namespace QueryInstitute.Data
{
    public static class SeedData
    {
        public static void Init(IApplicationBuilder builder)
        {
            using (var serviceScope = builder.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {

                var context = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();

                if (!context.Departments.Any())
                {
                    context.Departments.AddRange(Departments.Values);
                }


                if (!context.Courses.Any())
                {
                    var courseList = new List<Course>
                    {
                        new Course(){CourseName = "C#", Department = Departments["Programming"], Credits = 8},
                        new Course(){CourseName = "CCNA", Department = Departments["NetWork"], Credits = 8},
                        new Course(){CourseName = "HTML", Department = Departments["Design"], Credits = 8}
                    };
                    context.Courses.AddRange(courseList);
                }


                context.SaveChanges();
            }

        }

        private static Dictionary<string, Department> _departments;

        public static Dictionary<string, Department> Departments
        {
            get
            {
                if (_departments != null)
                {
                    return _departments;
                }

                Department[] deptList =
                {
                    new Department(){ DepartmentName = "Programming" },
                    new Department(){ DepartmentName = "Design" },
                    new Department(){ DepartmentName = "NetWork" },
                };

                _departments = new Dictionary<string, Department>();

                foreach (var department in deptList)
                {
                    _departments.Add(department.DepartmentName, department);
                }

                return _departments;
            }

        }
    }
}
