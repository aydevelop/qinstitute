﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;

namespace QueryInstitute.Data
{
    public class AccountInit : IAccountInitialize
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public AccountInit(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public void SeedData()
        {
            var adminRole = new IdentityRole("Admin");
            var userRole = new IdentityRole("User");

            if (!_roleManager.Roles.Any())
            {
                var roles = new List<IdentityRole>() { adminRole, userRole };
                foreach (var role in roles)
                {
                    _roleManager.CreateAsync(role).GetAwaiter().GetResult();
                }
            }

            if (_userManager.Users.Any()) return;

            var adminUser = new IdentityUser()
            {
                UserName = "admin",
                Email = "admin@gmail.com"
            };
            var normalUser = new IdentityUser()
            {
                UserName = "sem",
                Email = "sem@gmail.com"
            };

            _userManager.CreateAsync(adminUser, "P@ass0rd123").GetAwaiter().GetResult();
            _userManager.CreateAsync(normalUser, "P@ass0rd123").GetAwaiter().GetResult();

            _userManager.AddToRoleAsync(adminUser, adminRole.Name).GetAwaiter().GetResult();
            _userManager.AddToRoleAsync(normalUser, userRole.Name).GetAwaiter().GetResult();
        }
    }

    public interface IAccountInitialize
    {
        void SeedData();
    }
}
