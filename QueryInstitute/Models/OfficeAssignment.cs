﻿namespace QueryInstitute.Models
{
    public class OfficeAssignment
    {
        public string Location { get; set; }

        public int InstructorId { get; set; }
        public Instructor Instructor { get; set; }
    }
}
