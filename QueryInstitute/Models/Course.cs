﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace QueryInstitute.Models
{
    public class Course
    {
        public int CourseId { get; set; }

        [Required(ErrorMessage = "Please fill the coures title")]
        [Display(Name = "Course Title")]
        public string CourseName { get; set; }

        [Required(ErrorMessage = "Please fill the coures credits")]
        public int? Credits { get; set; }

        public int DepartmentId { get; set; }
        public Department Department { get; set; }

        public ICollection<Enrollment> Enrollments { get; set; }
        public ICollection<CourseAssignment> CourseAssignments { get; set; }


    }
}
