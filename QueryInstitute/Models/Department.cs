﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace QueryInstitute.Models
{
    public class Department
    {
        public int DepartmentId { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string DepartmentName { get; set; }

        [Required]
        public double Budget { get; set; }

        public ICollection<Course> Courses { get; set; }


        public int? InstructorId { get; set; }
        [Display(Name = "Adminstrator")]
        public Instructor Instructor { get; set; }
    }
}
