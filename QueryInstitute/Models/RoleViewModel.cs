﻿namespace QueryInstitute.Models
{
    public class RoleViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool Select { get; set; }
    }
}
