﻿using Microsoft.EntityFrameworkCore;
using QueryInstitute.Data;
using QueryInstitute.Models;
using QueryInstitute.Repository.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace QueryInstitute.Repository
{
    public class StudentRepository : Repository<Student>, IStudentRepository
    {
        public StudentRepository(ApplicationDbContext context) : base(context)
        {

        }

        public IEnumerable<Enrollment> CoursesToStudent(int studentId)
        {
            return context.Enrollments
               .Where(e => e.StudentId == studentId)
               .Include(x => x.Student)
               .Include(x => x.Course)
               .ToList();
        }
    }
}
