﻿using QueryInstitute.Data;
using QueryInstitute.Models;
using QueryInstitute.Repository.Contracts;

namespace QueryInstitute.Repository
{
    public class EnrollmentRepository : Repository<Enrollment>, IEnrollmentRepository
    {
        public EnrollmentRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}
