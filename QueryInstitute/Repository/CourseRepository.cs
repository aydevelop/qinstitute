﻿using Microsoft.EntityFrameworkCore;
using QueryInstitute.Data;
using QueryInstitute.Models;
using QueryInstitute.Repository.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace QueryInstitute.Repository
{
    public class CourseRepository : Repository<Course>, ICourseRepository
    {
        public CourseRepository(ApplicationDbContext context) : base(context)
        {

        }

        public IEnumerable<Course> CoursesToDeparment()
        {
            return context.Courses.Include(x => x.Department).ToList();
        }
    }
}
