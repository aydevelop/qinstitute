﻿using Microsoft.EntityFrameworkCore;
using QueryInstitute.Data;
using QueryInstitute.Models;
using QueryInstitute.Repository.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QueryInstitute.Repository
{
    public class InstructorRepository : Repository<Instructor>, IInstructorRepository
    {
        public InstructorRepository(ApplicationDbContext context) : base(context)
        {

        }

        public async Task<IEnumerable<Instructor>> InstructorsAsync()
        {
            return await context.Instructors
                .Include(o => o.OfficeAssignment)
                .Include(ca => ca.CourseAssignments)
                    .ThenInclude(c => c.Course)
                        .ThenInclude(d => d.Department)
                .Include(ca => ca.CourseAssignments)
                    .ThenInclude(c => c.Course)
                        .ThenInclude(s => s.Enrollments)
                            .ThenInclude(s => s.Student)
                .ToListAsync();
        }

        public async Task<Instructor> InstructorAsync(int id)
        {
            return await context.Instructors.Include(x => x.CourseAssignments)
                .ThenInclude(x => x.Course).FirstOrDefaultAsync(x => x.InstructorId == id);
        }
    }
}
