﻿using Microsoft.EntityFrameworkCore;
using QueryInstitute.Data;
using QueryInstitute.Models;
using QueryInstitute.Repository.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace QueryInstitute.Repository
{
    public class DepartmentRepository : Repository<Department>, IDepartmentRepository
    {
        public DepartmentRepository(ApplicationDbContext context) : base(context)
        { }

        public IEnumerable<Department> InstructorToDepartments()
        {
            return context.Departments.Include(i => i.Instructor).ToList();
        }

        public Department InstructorToDepartment(int id)
        {
            return context.Departments.Include(i => i.Instructor).FirstOrDefault(x => x.DepartmentId == id);
        }

    }
}
