﻿using Microsoft.EntityFrameworkCore;
using QueryInstitute.Data;
using QueryInstitute.Models;
using QueryInstitute.Repository.Contracts;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueryInstitute.Repository
{
    public class CourseAssignmentRepository : Repository<CourseAssignment>, ICourseAssignmentRepository
    {
        public CourseAssignmentRepository(ApplicationDbContext context) : base(context)
        {

        }

        public async Task<List<CourseAssignment>> CoursesToInstructorAsync(int id)
        {
            return await context.CourseAssignments
                .Where(x => x.InstructorId == id)
                .Include(x => x.Course).ToListAsync();
        }
    }
}
