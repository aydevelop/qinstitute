﻿using QueryInstitute.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QueryInstitute.Repository.Contracts
{
    public interface IInstructorRepository : IRepository<Instructor>
    {
        Task<IEnumerable<Instructor>> InstructorsAsync();
        Task<Instructor> InstructorAsync(int id);
    }
}
