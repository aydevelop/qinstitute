﻿using QueryInstitute.Models;
using System.Collections.Generic;

namespace QueryInstitute.Repository.Contracts
{
    public interface ICourseRepository : IRepository<Course>
    {
        IEnumerable<Course> CoursesToDeparment();
    }
}
