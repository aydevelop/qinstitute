﻿using QueryInstitute.Models;

namespace QueryInstitute.Repository.Contracts
{
    public interface IEnrollmentRepository : IRepository<Enrollment>
    {

    }
}
