﻿using QueryInstitute.Models;
using System.Collections.Generic;

namespace QueryInstitute.Repository.Contracts
{
    public interface IDepartmentRepository : IRepository<Department>
    {
        IEnumerable<Department> InstructorToDepartments();

        Department InstructorToDepartment(int id);
    }
}
