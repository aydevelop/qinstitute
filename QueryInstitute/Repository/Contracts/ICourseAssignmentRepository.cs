﻿using QueryInstitute.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QueryInstitute.Repository.Contracts
{
    public interface ICourseAssignmentRepository : IRepository<CourseAssignment>
    {
        Task<List<CourseAssignment>> CoursesToInstructorAsync(int id);
    }
}
