﻿using QueryInstitute.Models;
using System.Collections.Generic;

namespace QueryInstitute.Repository.Contracts
{
    public interface IStudentRepository : IRepository<Student>
    {
        IEnumerable<Enrollment> CoursesToStudent(int studentId);
    }
}
