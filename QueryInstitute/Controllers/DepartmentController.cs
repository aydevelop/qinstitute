﻿using Microsoft.AspNetCore.Mvc;
using QueryInstitute.Models;
using QueryInstitute.Repository.Contracts;

namespace QueryInstitute.Controllers
{
    public class DepartmentController : Controller
    {
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IInstructorRepository _instructorRepository;

        public DepartmentController(IDepartmentRepository departmentRepository, IInstructorRepository instructorRepository)
        {
            _departmentRepository = departmentRepository;
            _instructorRepository = instructorRepository;
        }

        public IActionResult Index()
        {
            var departments = _departmentRepository.InstructorToDepartments();
            return View(departments);
        }

        public IActionResult Details(int id)
        {
            var department = _departmentRepository.InstructorToDepartment(id);
            if (department == null)
            {
                return NotFound();
            }

            return View(department);
        }

        private void GetInstructors()
        {
            ViewBag.Instructors = _instructorRepository.GetAll();
        }

        public IActionResult Create()
        {
            GetInstructors();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Department model)
        {
            if (!ModelState.IsValid)
            {
                GetInstructors();
                return View();
            }

            _departmentRepository.Add(model);
            return RedirectToAction("Details", new { id = model.DepartmentId });
        }

        public IActionResult Edit(int id)
        {
            var department = _departmentRepository.GetById(id);
            if (department == null)
            {
                return NotFound();
            }

            GetInstructors();
            return View(department);
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public IActionResult EditPost(Department model)
        {
            if (!ModelState.IsValid)
            {
                GetInstructors();
                return View(model);
            }

            _departmentRepository.Update(model);
            return RedirectToAction("Details", new { id = model.DepartmentId });
        }

        public IActionResult Delete(int id)
        {
            return View();
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePost(int id)
        {
            var dep = _departmentRepository.GetById(id);
            if (dep == null)
            {
                return NotFound();
            }

            _departmentRepository.Delete(dep);
            return RedirectToAction("Index");
        }
    }
}
