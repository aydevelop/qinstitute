﻿using Microsoft.AspNetCore.Mvc;
using QueryInstitute.Models;
using QueryInstitute.Repository.Contracts;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QueryInstitute.Controllers
{
    public class InstructorController : Controller
    {
        private readonly IInstructorRepository _instructorRepository;
        private readonly ICourseRepository _courseRepository;
        private readonly ICourseAssignmentRepository _courseAssignmentRepository;

        public InstructorController(IInstructorRepository instructorRepository, ICourseRepository courseRepository, ICourseAssignmentRepository courseAssignmentRepository)
        {
            _instructorRepository = instructorRepository;
            _courseRepository = courseRepository;
            _courseAssignmentRepository = courseAssignmentRepository;
        }

        public async Task<IActionResult> Index(int? id, int? courseId)
        {
            var viewModel = new InstructorViewModel { Instructors = await _instructorRepository.InstructorsAsync() };

            if (id != null)
            {
                ViewData["InstructorID"] = id.Value;
                var instructor = viewModel.Instructors.Single(i => i.InstructorId == id.Value);
                viewModel.Courses = instructor.CourseAssignments.Select(s => s.Course);

                if (courseId == null) return View(viewModel);
                ViewData["CourseID"] = courseId.Value;
                viewModel.Enrollments = viewModel.Courses.Single(x => x.CourseId == courseId).Enrollments;
            }

            return View(viewModel);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var instructor = await _instructorRepository.InstructorAsync((int)id);
            return View(instructor);
        }

        public IActionResult Create()
        {
            var allCourses = _courseRepository.GetAll();

            var model = new InstructorCreateEditViewModel()
            {
                AssignedCourseData = allCourses.Select(c => new AssignedCourseData()
                {
                    CourseId = c.CourseId,
                    CourseName = c.CourseName,
                    Assigned = false
                }).ToList()
            };

            return View(model);
        }

        [HttpPost, ActionName("Create")]
        public IActionResult CreatePost(InstructorCreateEditViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (model.Instructor != null)
            {
                _instructorRepository.Add(model.Instructor);

                var iId = model.Instructor.InstructorId;
                foreach (var data in model.AssignedCourseData)
                {
                    if (data.Assigned)
                    {
                        _courseAssignmentRepository.Add(new CourseAssignment()
                        {
                            CourseId = data.CourseId,
                            InstructorId = iId
                        });
                    }
                }

                return RedirectToAction("Index");
            }


            return View("Create");
        }

        public async Task<IActionResult> Edit(int? id)
        {
            var instructor = await _instructorRepository.InstructorAsync(id ?? 0);
            if (instructor == null)
            {
                return NotFound();
            }


            var allCourses = _courseRepository.GetAll();
            var coursesToInstructor = await _courseAssignmentRepository.CoursesToInstructorAsync(instructor.InstructorId);
            var model = new InstructorCreateEditViewModel()
            {
                Instructor = instructor,
                AssignedCourseData = allCourses.Select(s => new AssignedCourseData()
                {
                    CourseId = s.CourseId,
                    CourseName = s.CourseName,
                    Assigned = coursesToInstructor.Exists(e => e.Course.CourseId == s.CourseId)
                }).ToList()
            };


            return View(model);
        }

        [HttpPost, ActionName("Edit")]
        public IActionResult EditPost(InstructorCreateEditViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return NotFound();
            }

            _instructorRepository.Update(model.Instructor);

            var cs = _courseAssignmentRepository.GetAll();
            foreach (var item in model.AssignedCourseData)
            {
                if (item.Assigned)
                {
                    var isExist = IsExistModel(_courseAssignmentRepository.GetAll(), model.Instructor.InstructorId, item.CourseId);
                    if (!isExist)
                    {
                        _courseAssignmentRepository.Add(new CourseAssignment()
                        {
                            CourseId = item.CourseId,
                            InstructorId = model.Instructor.InstructorId
                        });
                    }
                }
                else
                {
                    var isExist = IsExistModel(_courseAssignmentRepository.GetAll(), model.Instructor.InstructorId, item.CourseId);
                    var first = _courseAssignmentRepository.GetByFiler(x => x.InstructorId == model.Instructor.InstructorId
                    && x.CourseId == item.CourseId).FirstOrDefault();
                    if (first != null)
                    {
                        _courseAssignmentRepository.Delete(first);
                    }
                }
            }

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Delete(int? id)
        {
            var model = await _instructorRepository.InstructorAsync(id ?? 0);
            if (model == null)
            {
                return NotFound();
            }

            return View(model);
        }
        [HttpPost, ActionName("Delete")]
        public IActionResult DeletePost(int? InstructorId)
        {
            var instructor = _instructorRepository.GetById(InstructorId ?? 0);
            if (instructor == null)
            {
                return NotFound();
            }

            _instructorRepository.Delete(instructor);

            return RedirectToAction("Index");
        }

        private bool IsExistModel(IEnumerable<CourseAssignment> source, int instructorId, int courseId)
        {
            return source.Where(x => x.InstructorId == instructorId).Any(c => c.CourseId == courseId);
        }

    }
}
