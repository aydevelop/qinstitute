﻿using Microsoft.AspNetCore.Mvc;

namespace QueryInstitute.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return Redirect("/Course");
        }
    }
}
