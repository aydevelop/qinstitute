﻿using Microsoft.AspNetCore.Mvc;
using QueryInstitute.Models;
using QueryInstitute.Repository.Contracts;
using System.Linq;

namespace QueryInstitute.Controllers
{
    public class CourseController : Controller
    {
        private readonly ICourseRepository _courseRepository;
        private readonly IDepartmentRepository _departmentRepository;

        public CourseController(ICourseRepository courseRepository, IDepartmentRepository departmentRepository)
        {
            _courseRepository = courseRepository;
            _departmentRepository = departmentRepository;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var courses = _courseRepository.CoursesToDeparment();
            return View(courses);
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.Dep = _departmentRepository.GetAll();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Course model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Dep = _departmentRepository.GetAll();
                return View(model);
            }

            _courseRepository.Add(model);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var course = _courseRepository.GetById(id);
            if (course == null)
            {
                return NotFound();
            }

            ViewBag.Dep = _departmentRepository.GetAll();
            return View(course);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Course model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Dep = _departmentRepository.GetAll();
                return View(model);
            }

            _courseRepository.Update(model);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            var course = _courseRepository.GetById(id);
            if (course == null)
            {
                return NotFound();
            }

            return View(course);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteCourse(int CourseId)
        {
            var course = _courseRepository.GetById(CourseId);
            if (course == null)
            {
                return NotFound();
            }

            _courseRepository.Delete(course);
            return RedirectToAction("Index");
        }

        public IActionResult Details(int id)
        {
            var course = _courseRepository.CoursesToDeparment().FirstOrDefault(x => x.CourseId == id);
            if (course == null)
            {
                return NotFound();
            }

            return View(course);
        }
    }
}
