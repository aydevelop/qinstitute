﻿using Microsoft.AspNetCore.Mvc;
using QueryInstitute.Models;
using QueryInstitute.Repository.Contracts;
using System.Linq;

namespace QueryInstitute.Controllers
{
    public class StudentController : Controller
    {
        private readonly IStudentRepository _studentRepository;
        private readonly IEnrollmentRepository _enrollmentRepository;
        private readonly ICourseRepository _coursesRepository;

        public StudentController(IStudentRepository studentRepository, IEnrollmentRepository enrollmentRepository, ICourseRepository coursesRepository)
        {
            _studentRepository = studentRepository;
            _enrollmentRepository = enrollmentRepository;
            _coursesRepository = coursesRepository;
        }

        public IActionResult Index(string sort, string searchString)
        {
            var students = _studentRepository.GetAll().Reverse();

            ViewData["sortName"] = "desc";
            if (sort == "asc")
            {
                ViewData["sortName"] = "desc";
                students = students.OrderByDescending(s => s.EnrollmentDate);
            }
            else if (sort == "desc")
            {
                ViewData["sortName"] = "asc";
                students = students.OrderBy(s => s.EnrollmentDate);
            }

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                ViewData["currentFilter"] = searchString;
                students = students.Where(s => s.FirstName.ToLower().Contains(searchString.ToLower())
                || s.LastName.ToLower().Contains(searchString.ToLower()));
            }

            return View(students);
        }

        public IActionResult Details(int id = 0)
        {
            ViewBag.Courses = _coursesRepository.GetAll();
            var student = _studentRepository.GetById(id);
            if (student == null)
            {
                return NotFound();
            }

            var model = new StudentViewModel()
            {
                Student = student,
                Enrollments = _studentRepository.CoursesToStudent(student.StudentId)

            };

            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Student model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            _studentRepository.Add(model);
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            var student = _studentRepository.GetById(id);
            if (student == null)
            {
                return NotFound();
            }
            return View(student);
        }

        [HttpPost, ActionName("Delete")]
        [AutoValidateAntiforgeryToken]
        public IActionResult DeletePost(int StudentId)
        {
            var student = _studentRepository.GetById(StudentId);
            if (student == null)
            {
                return NotFound();
            }

            _studentRepository.Delete(student);
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var course = _studentRepository.GetById(id);
            if (course == null)
            {
                return NotFound();
            }

            return View(course);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Student model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            _studentRepository.Update(model);
            return RedirectToAction(nameof(Index));
        }

        public IActionResult AddCourseToStudent(StudentViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Enrollment.StudentId == 0 || model.Enrollment.CourseId == 0)
                {

                    return RedirectToAction("Index");
                }
                _enrollmentRepository.Add(model.Enrollment);
            };
            return RedirectToAction("Details", routeValues: new { id = model.Enrollment.StudentId });
        }
    }
}
